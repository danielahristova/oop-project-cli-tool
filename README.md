# OOP Project - CLI tool

> It’s the job that’s never started as takes longest to finish.

— Sam Gamgee


## Precious 
**precious** is a CLI tool for all "The Lord of the Rings" lovers out there.
You can get information about the books, the movie trilogies or refresh your memory with some random quotes.
