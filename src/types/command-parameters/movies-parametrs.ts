export interface MoviesCommandParameters {
    part?: number,
    d?: boolean,
    b?: boolean,
    n?: boolean, 
    a?: boolean
  }