export interface BooksCommandParameters {
    part?: number,
    chapters?: boolean
  }