
import { BooksCommandParameters } from './books-parameters';
import { MoviesCommandParameters } from './movies-parametrs';

// merge command parameter types
export type CommandParameters = BooksCommandParameters & MoviesCommandParameters;