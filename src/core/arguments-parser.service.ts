import { IArgumentsParser } from './../types/core/arguments-parser';
import { CommandParameters } from './../types/command-parameters/command-parameters';
import minimist from 'minimist';
import { join } from 'path';

export class ArgumentsParser implements IArgumentsParser {

  private readonly _arguments: CommandParameters;
  private readonly _command: string;

  constructor() {
    const args = minimist(process.argv.slice(2));

    // get the command name
    this._command = args._[0];

    // initialize the parsed _arguments object
    this._arguments = {};

    // parse and validate individual _arguments
    /* your code here */
    if (args.chapters) {
      this._arguments.chapters = true;
    }
    if (!Number.isNaN(+args.part)) {
      this._arguments.part = +args.part;
    }
    if (args.d) {
      this._arguments.d = true;
    }
    if (args.b) {
      this._arguments.b = true;
    }
    if (args.n) {
      this._arguments.n = true;
    }
    if (args.a) {
      this._arguments.a = true;
    }
  }

  public get arguments(): CommandParameters {
    return this._arguments;
  }

  public get command(): string {
    return this._command;
  }

}