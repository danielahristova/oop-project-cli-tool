import { ExecutionResult } from "../types/execution-result";
import { ICommand } from "../types/command";
import { ConsolePrinter } from "../core/console-printer.service";
import { FormatterService } from "../core/formatter.service";
import { ColorType } from "../common/colors";
import { BASE_URL } from "../common/base-url";
import { API_KEY } from "../common/api-key";
import fetch from "node-fetch";
import { MoviesCommandParameters } from "../types/command-parameters/movies-parametrs";

export class MoviesCommand implements ICommand {

    constructor(

      private readonly printer: ConsolePrinter = new ConsolePrinter(),
      private readonly formatter: FormatterService = new FormatterService(),
      
    ) { }
  
    public async execute({part = undefined, d= false, b = false, n = false, a =false}: MoviesCommandParameters): Promise<ExecutionResult> {
      try{
        
        const response = await fetch(`${BASE_URL}/movie`, {
          method: 'GET',
          headers: {
              'Authorization': `Bearer ${API_KEY}`,
          }
      });;
        const result = await response.json();        

        if(part == undefined){
        result.docs.forEach((movies: any)=> this.printer.print(movies.name));
        } else {
          this.validateParams(part);
          let selectedPart
          switch (part) {
          case 1: selectedPart =  result.docs[6]; break;
          case 2: selectedPart =  result.docs[5]; break;
          case 3: selectedPart =  result.docs[7]; break;
          case 4: selectedPart =  result.docs[2]; break;
          case 5: selectedPart =  result.docs[3]; break;
          case 6: selectedPart =  result.docs[4]
          }
          this.printer.print(selectedPart.name);
          if (d){
            this.printer.print(`The selected movie is ${selectedPart.runtimeInMinutes} minutes long.`);
          }
          if (b){
            this.printer.print(`The budget for this part was ${selectedPart.budgetInMillions} millions.`);
          }
          if (n){
            this.printer.print(`Number of Oscars Nominations: ${selectedPart.academyAwardNominations}.`);
          }
          if (a){
            this.printer.print(`Oscars this movie has won: ${selectedPart.academyAwardWins}.`);
          }
        }
    return { errors: 0, message: undefined };
      } catch (error) {
        return {
          errors: 1,
          message: this.formatter.format(error.message, ColorType.Red),
        };
      }
    }
  
    private validateParams(part: number) {
      if (part < 1 || part > 6) {
        throw new RangeError(`Movie part should be between 1 and 6, got ${part} instead!`);
      }
    }
  }