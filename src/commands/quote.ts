import { ICommand } from "../types/command";
import { ExecutionResult } from "../types/execution-result";
import { ConsolePrinter } from "../core/console-printer.service";
import { ColorType } from "../common/colors";
import { BASE_URL } from "../common/base-url";
import { API_KEY } from "../common/api-key";
import { FormatterService } from "../core/formatter.service";
import fetch from "node-fetch";

export class QuoteCommand implements ICommand {
  constructor(
    private readonly printer: ConsolePrinter = new ConsolePrinter(),
    private readonly formatter: FormatterService = new FormatterService(),
  ) { }

  public async execute(): Promise<ExecutionResult> {
    try {
      const quotes = await fetch(`${BASE_URL}/quote`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${API_KEY}`,
        }
    });
      const result = await quotes.json();

      const randomNumber = Math.floor(Math.random() * (result.docs.length));
      const element = result.docs[randomNumber];

      const whoSaidIt = await fetch(`${BASE_URL}/character/${element.character}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${API_KEY}`,
        }
    });
      const resp = await whoSaidIt.json();

      const movie = await fetch(`${BASE_URL}/movie/${element.movie}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${API_KEY}`,
        }
    });
      const whichMovie = await movie.json();

      this.printer.print(`"${element.dialog}" - ${resp.name} in "${whichMovie.name}"`);
      return { errors: 0, message: undefined };
    } catch (error) {
      return {
        errors: 1,
        message: this.formatter.format(error.message, ColorType.Red),
      };
    }
  }
}