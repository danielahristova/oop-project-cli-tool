import { ICommand } from "../types/command";
import { ExecutionResult } from "../types/execution-result";
import { BooksCommandParameters } from "../types/command-parameters/books-parameters";
import { ConsolePrinter } from "../core/console-printer.service";
import { FormatterService } from "../core/formatter.service";
import { ColorType } from "../common/colors";
import { BASE_URL } from "../common/base-url";
import fetch from "node-fetch";

export class BooksCommand implements ICommand {


  constructor(
    private readonly consolePrinter: ConsolePrinter = new ConsolePrinter(),
    private readonly formatter: FormatterService = new FormatterService(),
  ) { }

  public async execute({ part = undefined, chapters = false }: BooksCommandParameters): Promise<ExecutionResult> {
    try {
      
      const response = await fetch(`${BASE_URL}/book`);
      const result = await response.json();
      
      if (part == undefined) {
        result.docs.forEach((books: any) => this.consolePrinter.print(books.name));
      } else {
        this.validateParams(part);
        this.consolePrinter.print(result.docs[part - 1].name);
        if (chapters) {
          const chaptersRequest = await fetch(`${BASE_URL}/book/${result.docs[part - 1]._id}/chapter`);
          const chaptersData = await chaptersRequest.json();
          this.consolePrinter.print(`The selected book contains ${chaptersData.items.total} chapters.`)
        }
      }
      return { errors: 0, message: undefined };
    } catch (error) {
      return {
        errors: 1,
        message: this.formatter.format(error.message, ColorType.Red),
      };
    }
  }
  private validateParams(part: number) {
    if (part < 1 || part > 3) {
      throw new RangeError(`Book part should be between 1 and 3, got ${part} instead!`);
    }
  }
}
