import { HelpCommand } from "./help";
import { BooksCommand } from "./books";
import { MoviesCommand } from "./movies";
import { QuoteCommand } from "./quote";

export class CommandContainer {
  
  constructor(
    /* Your commands here */
    public readonly help: HelpCommand = new HelpCommand(),
    public readonly books: BooksCommand = new BooksCommand(),
    public readonly movies: MoviesCommand = new MoviesCommand(),
    public readonly quote: QuoteCommand = new QuoteCommand()
    ) { }

}