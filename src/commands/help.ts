import { ConsolePrinter } from './../core/console-printer.service';
import { ICommand } from './../types/command';
import { ExecutionResult } from '../types/execution-result';

export class HelpCommand implements ICommand {

  constructor(
    private readonly printer: ConsolePrinter = new ConsolePrinter(),
  ) { }

  public async execute(): Promise<ExecutionResult> {
    this.printer.print(
      `
      Usage: precious <command>

      where <command> could be replaced by one of:
        help, books, movies, quote

      precious help               prints this manual

      precious books              prints all the books from the series
        arguments:
          --part=<number>         prints the selected book part name
          --chapters              prints the number of chapters there are in the selected book part

        example:
          precious books --part=1 --chapters
          
      precious movies             prints all movie titles from the series, including "The Hobbit" trilogies
        arguments:
          --part=<number>         prints the selected movie title
          --d                     duration of the selected part
          --b                     budget for the selected part
          --n                     number of award nominations
          --a                     number of awards the movie has won

        example:
          precious movies --part=2 --d --b --n --a 
        
      precious quote              prints a random quote
      
        example:
          precious quote 
      `
    );

    return { errors: 0, message: undefined };
  }

}