"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const console_printer_service_1 = require("./../core/console-printer.service");
class HelpCommand {
    constructor(printer = new console_printer_service_1.ConsolePrinter()) {
        this.printer = printer;
    }
    execute() {
        return __awaiter(this, void 0, void 0, function* () {
            this.printer.print(`
      Usage: precious <command>

      where <command> could be replaced by one of:
        help, books, movies, quote

      precious help               prints this manual

      precious books              prints all the books from the series
        arguments:
          --part=<number>         prints the selected book part name
          --chapters              prints the number of chapters there are in the selected book part

        example:
          precious books --part=1 --chapters
          
      precious movies             prints all movie titles from the series, including "The Hobbit" trilogies
        arguments:
          --part=<number>         prints the selected movie title
          --d                     duration of the selected part
          --b                     budget for the selected part
          --n                     number of award nominations
          --a                     number of awards the movie has won

        example:
          precious movies --part=2 --d --b --n --a 
        
      precious quote              prints a random quote
      
        example:
          precious quote 
      `);
            return { errors: 0, message: undefined };
        });
    }
}
exports.HelpCommand = HelpCommand;
