"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const console_printer_service_1 = require("../core/console-printer.service");
const colors_1 = require("../common/colors");
const base_url_1 = require("../common/base-url");
const api_key_1 = require("../common/api-key");
const formatter_service_1 = require("../core/formatter.service");
const node_fetch_1 = __importDefault(require("node-fetch"));
class QuoteCommand {
    constructor(printer = new console_printer_service_1.ConsolePrinter(), formatter = new formatter_service_1.FormatterService()) {
        this.printer = printer;
        this.formatter = formatter;
    }
    execute() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const quotes = yield node_fetch_1.default(`${base_url_1.BASE_URL}/quote`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${api_key_1.API_KEY}`,
                    }
                });
                const result = yield quotes.json();
                const randomNumber = Math.floor(Math.random() * (result.docs.length));
                const element = result.docs[randomNumber];
                const whoSaidIt = yield node_fetch_1.default(`${base_url_1.BASE_URL}/character/${element.character}`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${api_key_1.API_KEY}`,
                    }
                });
                const resp = yield whoSaidIt.json();
                const movie = yield node_fetch_1.default(`${base_url_1.BASE_URL}/movie/${element.movie}`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${api_key_1.API_KEY}`,
                    }
                });
                const whichMovie = yield movie.json();
                this.printer.print(`"${element.dialog}" - ${resp.name} in "${whichMovie.name}"`);
                return { errors: 0, message: undefined };
            }
            catch (error) {
                return {
                    errors: 1,
                    message: this.formatter.format(error.message, colors_1.ColorType.Red),
                };
            }
        });
    }
}
exports.QuoteCommand = QuoteCommand;
