"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const console_printer_service_1 = require("../core/console-printer.service");
const formatter_service_1 = require("../core/formatter.service");
const colors_1 = require("../common/colors");
const base_url_1 = require("../common/base-url");
const node_fetch_1 = __importDefault(require("node-fetch"));
class BooksCommand {
    constructor(consolePrinter = new console_printer_service_1.ConsolePrinter(), formatter = new formatter_service_1.FormatterService()) {
        this.consolePrinter = consolePrinter;
        this.formatter = formatter;
    }
    execute({ part = undefined, chapters = false }) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield node_fetch_1.default(`${base_url_1.BASE_URL}/book`);
                const result = yield response.json();
                if (part == undefined) {
                    result.docs.forEach((books) => this.consolePrinter.print(books.name));
                }
                else {
                    this.validateParams(part);
                    this.consolePrinter.print(result.docs[part - 1].name);
                    if (chapters) {
                        const chaptersRequest = yield node_fetch_1.default(`${base_url_1.BASE_URL}/book/${result.docs[part - 1]._id}/chapter`);
                        const chaptersData = yield chaptersRequest.json();
                        this.consolePrinter.print(`The selected book contains ${chaptersData.items.total} chapters.`);
                    }
                }
                return { errors: 0, message: undefined };
            }
            catch (error) {
                return {
                    errors: 1,
                    message: this.formatter.format(error.message, colors_1.ColorType.Red),
                };
            }
        });
    }
    validateParams(part) {
        if (part < 1 || part > 3) {
            throw new RangeError(`Book part should be between 1 and 3, got ${part} instead!`);
        }
    }
}
exports.BooksCommand = BooksCommand;
