"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const help_1 = require("./help");
const books_1 = require("./books");
const movies_1 = require("./movies");
const quote_1 = require("./quote");
class CommandContainer {
    constructor(
    /* Your commands here */
    help = new help_1.HelpCommand(), books = new books_1.BooksCommand(), movies = new movies_1.MoviesCommand(), quote = new quote_1.QuoteCommand()) {
        this.help = help;
        this.books = books;
        this.movies = movies;
        this.quote = quote;
    }
}
exports.CommandContainer = CommandContainer;
