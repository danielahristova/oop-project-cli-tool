"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const console_printer_service_1 = require("../core/console-printer.service");
const formatter_service_1 = require("../core/formatter.service");
const colors_1 = require("../common/colors");
const base_url_1 = require("../common/base-url");
const api_key_1 = require("../common/api-key");
const node_fetch_1 = __importDefault(require("node-fetch"));
class MoviesCommand {
    constructor(printer = new console_printer_service_1.ConsolePrinter(), formatter = new formatter_service_1.FormatterService()) {
        this.printer = printer;
        this.formatter = formatter;
    }
    execute({ part = undefined, d = false, b = false, n = false, a = false }) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield node_fetch_1.default(`${base_url_1.BASE_URL}/movie`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${api_key_1.API_KEY}`,
                    }
                });
                ;
                const result = yield response.json();
                if (part == undefined) {
                    result.docs.forEach((movies) => this.printer.print(movies.name));
                }
                else {
                    this.validateParams(part);
                    let selectedPart;
                    switch (part) {
                        case 1:
                            selectedPart = result.docs[6];
                            break;
                        case 2:
                            selectedPart = result.docs[5];
                            break;
                        case 3:
                            selectedPart = result.docs[7];
                            break;
                        case 4:
                            selectedPart = result.docs[2];
                            break;
                        case 5:
                            selectedPart = result.docs[3];
                            break;
                        case 6: selectedPart = result.docs[4];
                    }
                    this.printer.print(selectedPart.name);
                    if (d) {
                        this.printer.print(`The selected movie is ${selectedPart.runtimeInMinutes} minutes long.`);
                    }
                    if (b) {
                        this.printer.print(`The budget for this part was ${selectedPart.budgetInMillions} millions.`);
                    }
                    if (n) {
                        this.printer.print(`Number of Oscars Nominations: ${selectedPart.academyAwardNominations}.`);
                    }
                    if (a) {
                        this.printer.print(`Oscars this movie has won: ${selectedPart.academyAwardWins}.`);
                    }
                }
                return { errors: 0, message: undefined };
            }
            catch (error) {
                return {
                    errors: 1,
                    message: this.formatter.format(error.message, colors_1.ColorType.Red),
                };
            }
        });
    }
    validateParams(part) {
        if (part < 1 || part > 6) {
            throw new RangeError(`Movie part should be between 1 and 6, got ${part} instead!`);
        }
    }
}
exports.MoviesCommand = MoviesCommand;
