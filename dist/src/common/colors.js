"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ColorType;
(function (ColorType) {
    ColorType["White"] = "#FFFFFF";
    ColorType["Black"] = "#000000";
    ColorType["Red"] = "#FF0000";
    ColorType["Green"] = "#00FF00";
    ColorType["Blue"] = "#0000FF";
    ColorType["Yellow"] = "#FFFF00";
    ColorType["Purple"] = "#FF00FF";
})(ColorType = exports.ColorType || (exports.ColorType = {}));
