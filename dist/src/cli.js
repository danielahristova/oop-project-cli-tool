"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const arguments_parser_service_1 = require("./core/arguments-parser.service");
const commands_container_1 = require("./commands/commands-container");
const console_printer_service_1 = require("./core/console-printer.service");
class CLI {
    constructor(parser = new arguments_parser_service_1.ArgumentsParser(), printer = new console_printer_service_1.ConsolePrinter(), commandContainer = new commands_container_1.CommandContainer()) {
        this.parser = parser;
        this.printer = printer;
        this.commandContainer = commandContainer;
    }
    main() {
        return __awaiter(this, void 0, void 0, function* () {
            const command = this.parser.command;
            const args = this.parser.arguments;
            if (this.commandContainer[command] && typeof this.commandContainer[command].execute === 'function') {
                const executionResult = yield this.commandContainer[command].execute(args);
                if (executionResult.errors) {
                    this.printer.print(executionResult.message);
                }
            }
        });
    }
}
exports.CLI = CLI;
