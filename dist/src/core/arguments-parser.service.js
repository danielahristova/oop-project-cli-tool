"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const minimist_1 = __importDefault(require("minimist"));
class ArgumentsParser {
    constructor() {
        const args = minimist_1.default(process.argv.slice(2));
        // get the command name
        this._command = args._[0];
        // initialize the parsed _arguments object
        this._arguments = {};
        // parse and validate individual _arguments
        /* your code here */
        if (args.chapters) {
            this._arguments.chapters = true;
        }
        if (!Number.isNaN(+args.part)) {
            this._arguments.part = +args.part;
        }
        if (args.d) {
            this._arguments.d = true;
        }
        if (args.b) {
            this._arguments.b = true;
        }
        if (args.n) {
            this._arguments.n = true;
        }
        if (args.a) {
            this._arguments.a = true;
        }
    }
    get arguments() {
        return this._arguments;
    }
    get command() {
        return this._command;
    }
}
exports.ArgumentsParser = ArgumentsParser;
