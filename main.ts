#!/usr/bin/env node

import 'reflect-metadata';

import { CLI } from './src/cli';

async function bootstrap() {
  const app = new CLI();
  // console.log(`this is main.ts`);
  

  await app.main();
}

bootstrap();